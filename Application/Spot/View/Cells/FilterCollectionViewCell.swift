

import UIKit

class FilterCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var subheading: UILabel!
    @IBOutlet weak var background: UIView!
    @IBOutlet weak var gradient: UIImageView!
}
